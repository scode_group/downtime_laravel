<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{


    public $successStatus = 200;


    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->accessToken;
            return response()->json(['token' => $token, 'username' => $user['username'], 'user_status' => $user['user_status'], 'emp_id' => $user['emp_id'], 'success'=>true], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'เบอร์โทรหรือรหัสผ่านผิด', 'success'=>false], 200);
        }
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            // 'email' => 'required|email',
            'password' => 'required',
            // 'c_password' => 'required|same:password',
            'user_status' => 'required',
            'emp_id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);       
        }


        $input = $request->all();
        $users = DB::table('users')
            ->where('users.username', '=', $input['username'])
            ->count();
        if ($users >= 1){
            $success['token'] =  null;
            $success['name'] =  'เบอร์โทรซ้ำ';
        } else {
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            
            function RandomString()
            {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                $randstring = $characters[rand(0, strlen($characters)-1)];
                return $randstring;
            }
            $password = '';
            for ($i = 0; $i < 10; $i++){
                $rand = RandomString();
                $password .= $rand;
            }

            $emp_id = DB::select("SELECT MAX(emp_id) FROM `employee`");
            foreach ($emp_id as $key => $value){
                foreach ($value as $key2 => $value2){
                    $max_empID = $value2;
                }
            }
            $emp_id = $max_empID + 1;

            DB::table('new_user')->insert([
                [   'id' => null, 
                    'phone_number' => $input['username'],
                    'confirm' => $password,
                    'dalete' => '0',
                ]
            ]);

            DB::table('employee')->insert([
                ['emp_id' => $emp_id,
                'EnNo' => null, 
                'emp_sex' => null,
                'emp_name' => null, 
                'emp_lastname' => null,
                'emp_nickname' => null,
                'emp_address' => null,
                'emp_birthday' => null,
                'emp_weight' => null,
                'emp_height' => null,
                'emp_img' => null,
                'emp_headshot' => null,
                'emp_compcard' => null,
                'emp_routine' => 1,
                'emp_still' => 1,
                'emp_data' => 0,]
            ]);

            DB::table('star')->insert([
                ['star_id' => null,
                'star_1' => 0, 
                'star_2' => 0,
                'star_3' => 0,
                'star_4' => 0,
                'star_5' => 0,
                'emp_id' => $emp_id,]
            ]);
            $username = $input['username'];
            DB::select("UPDATE `users` SET `emp_id` = $emp_id
                 WHERE `users`.`username` = $username;");
            
        }



        return response()->json(['success'=>$success, 'users'=>$users], $this->successStatus);
    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        // $this->guard()->logout();
        Auth::guard('api')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        $json = [
            'success' => true,
            'code' => 200,
            'message' => 'You are Logged out.',
        ];
        return response()->json($json, '200');
    }

    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_status' => 'required',
            'name' => 'required',
            'username' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();

        // $user = User::create($input);
        $users = DB::table('users')
            ->where('users.username', '=', $input['username'])
            ->count();
        if ($users > 0){
            $message =  'เบอร์โทรซ้ำ';
            return response()->json(['message'=>$message, 'data'=>null, 'status'=>false], 200);
        } else {
            DB::table('users')->insert([
                [   'id' => null, 
                    'name' => $input['name'], 
                    'username' => $input['username'],
                    'password' => null,
                    'user_status' => $input['user_status'],
                    'emp_id' => null,
                    'remember_token' => null,
                    'created_at' => null,
                    'updated_at' => null,
                ]
            ]);
            $message =  'สร้าง User สำเร็จ';
            return response()->json(['message'=>$message, 'data'=>$input, 'status'=>true], 200);
        }
    }

    public function allUser(Request $request)
    {
        $users = DB::table('users')
            // ->leftJoin('new_user', 'users.usernamne', '=', 'new_user.phone_number')
            ->select('users.id', 'users.username', 'users.name', 'users.user_status')
            ->get();
        return response()->json(['message'=>'allUser', 'users'=>$users], 200);
    }

    public function allEmp(Request $request)
    {
        $user = Auth::user();
        $user_status = $user->user_status;
        $emp_id = $user->emp_id;
        if ($user_status >= 3){
            $users = DB::table('users')
                ->leftJoin('employee', 'users.emp_id', '=', 'employee.emp_id')
                ->leftJoin('star', 'users.emp_id', '=', 'star.emp_id')
                ->select('employee.*', 'star.*', 'users.id', 'users.username', 'users.name', 'users.user_status')
                ->where('users.user_status', '<=', 2)
                ->get();
            $status = 200;
            $message = 'user_status Ok';
        }else {
            $users = null;
            $status = 400;
            $message = 'user_status ไม่ผ่าน';
        }
        return response()->json(['message'=>$message, 'users'=>$users], $status);
    }

    public function allEmp_table(Request $request)
    {
        $user = Auth::user();
        $user_status = $user->user_status;
        $emp_id = $user->emp_id;
        if ($user_status >= 3){
            $users = DB::table('users')
                ->leftJoin('employee', 'users.emp_id', '=', 'employee.emp_id')
                ->leftJoin('star', 'users.emp_id', '=', 'star.emp_id')
                ->select('employee.*', 'star.*', 'users.id', 'users.username', 'users.name', 'users.user_status')
                // ->where('users.user_status', '<=', 2)
                ->where([
                    ['users.user_status', '<=', 2],
                    ['employee.emp_data', '=', 1],
                ])
                ->get();
            $status = 200;
            $message = 'user_status Ok';
        }else {
            $users = null;
            $status = 400;
            $message = 'user_status ไม่ผ่าน';
        }
        return response()->json(['message'=>$message, 'users'=>$users], $status);
    }

    public function allManager(Request $request)
    {
        $users = DB::table('users')
            ->select('users.id', 'users.username', 'users.name', 'users.user_status')
            ->where('users.user_status', '=', 3)
            ->get();
        return response()->json(['message'=>'allUser', 'users'=>$users], 200);
    }

    public function allAdmin(Request $request)
    {
        $users = DB::table('users')
            ->select('users.id', 'users.username', 'users.name', 'users.user_status')
            ->where('users.user_status', '=', 4)
            ->get();
        return response()->json(['message'=>'allUser', 'users'=>$users], 200);
    }

    public function de_user(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $number = $input['number'];
        // DB::select("DELETE FROM `users` WHERE `users`.`id` = '$id'");
        $users = DB::table('users')
            ->select('users.emp_id')
            ->where('users.username', '=', $number)
            ->get();
        foreach ($users as $key => $value){
            foreach ($value as $key2 => $value2){
                $emp_id = $value2;
            }
        }
        if($emp_id != null){
            DB::select("DELETE FROM `strike` WHERE `strike`.`emp_id` = '$emp_id'");
            DB::select("DELETE FROM `task_user` WHERE `task_user`.`emp_id` = '$emp_id'");
            DB::select("DELETE FROM `star` WHERE `star`.`emp_id` = '$emp_id'");
            DB::select("DELETE FROM `users` WHERE `users`.`username` = '$number'");
            DB::select("DELETE FROM `new_user` WHERE `new_user`.`phone_number` = '$number'");
            DB::select("DELETE FROM `employee` WHERE `employee`.`emp_id` = '$emp_id'");
        }else {
            DB::select("DELETE FROM `users` WHERE `users`.`username` = '$number'");
            DB::select("DELETE FROM `new_user` WHERE `new_user`.`phone_number` = '$number'");
        }
        return response()->json(['message'=>$emp_id], 200);
    }

    public function up_Status(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        DB::select("UPDATE `users` SET `user_status` = '2' WHERE `users`.`id` = '$id'");
        return response()->json(['message'=>'up_Status สำเร็จ'], 200);
    }

    public function down_Status(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        DB::select("UPDATE `users` SET `user_status` = '1' WHERE `users`.`id` = '$id'");
        return response()->json(['message'=>'down_Status สำเร็จ'], 200);
    }

    public function sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt' => 'required',
            'number' => 'required',
            'confirm' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
            'api_key' => '6484bab4',
            'api_secret' => 'WP54bLyNbBnVdhBl',
            // 'to' => '66842229026',
            'to' => $input['number'],
            'from' => 'Scode',
            'text' => $input['txt']."\r\n".'code: ' . $input['confirm']
        ]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        // $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
        //     'api_key' => '6484bab4',
        //     'api_secret' => 'WP54bLyNbBnVdhBl',
        //     // 'to' => '66842229026',
        //     'to' => $input['number'],
        //     'from' => 'Scode',
        //     'text' => 'code: ' . $input['confirm']
        // ]);
        // $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $response = curl_exec($ch);


        $json = [
            'success' => true,
            'code' => 200,
            'message' => 'smsApi',
            'response' => $response,
        ];
        return response()->json($json, '200');
    }

    public function confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $number = $input['number'];

        $users = DB::table('new_user')
            ->select('confirm')
            ->where('new_user.phone_number', '=', $number)
            ->get();

        $json = [
            'success' => true,
            'code' => 200,
            'message' => 'smsApiasdasd',
            'response' => $users,
        ];
        return response()->json($json, '200');
    }

    public function confirm2(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number' => 'required',
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $number = $input['number'];
        $code = $input['code'];

        $confirm = DB::table('new_user')
            ->select('confirm')
            ->where('new_user.phone_number', '=', $number)
            ->where('new_user.confirm', '=', $code)
            ->get();

        if (count($confirm) > 0) {
            $success = true;
        }else {
            $success = false;
        }

        $json = [
            'success' => $success,
        ];
        return response()->json($json, '200');
    }

    public function test(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        return response()->json($input, '200');
    }

    public function testToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $user = Auth::user();
        $emp_id = $user->username;
        $input['user'] = $user;
        $emp_data = DB::table('employee')
            ->select('emp_data')
            ->where('employee.emp_id', '=', $emp_id)
            ->get();
        $emp_data = $emp_data[0];
        return response()->json($emp_data, '200');
    }

    public function newPassApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $new_password = bcrypt($input['password']);
        $number = $input['number'];

        $data = DB::select("UPDATE `users` SET `password` = '$new_password' WHERE `username` = '$number';");
        $input['data'] = $data;
        
        return response()->json($input, '200');
    }

    public function uploadImg(Request $request){
        // var_dump($request->all());

        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $file2 = $request->file('Image2');
            $file3 = $request->file('Image3');
            // $user = Auth::user();
            // $emp_id = $user->emp_id;
            $emp_id = $request->emp_id;
            $users = DB::table('users')
                ->select('users.emp_id')
                ->where('users.username', '=', $emp_id)
                ->get();
            $emp_id = $users[0]->emp_id;
            // var_dump ($emp_id);
            // var_dump ($file);
            $UploadFile = Storage::disk('local')->putFile("headshot", $file);
            // echo $UploadFile."\n";
            if ($UploadFile) {
                // echo "สร้างไฟล์ $UploadFile สำเร็จ \n";
                $employee = DB::table('employee')
                    ->select('emp_headshot')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                // var_dump($employee[0]->emp_headshot);
                $emp_headshot = $employee[0]->emp_headshot;
                // echo "$emp_headshot";
                Storage::delete($emp_headshot);
            }else {
                // echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                // echo "เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
                $data = DB::select("UPDATE `employee` SET `emp_headshot` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                // echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }

            $UploadFile = Storage::disk('local')->putFile("img", $file2);
            // echo $UploadFile."\n";
            if ($UploadFile) {
                // echo "สร้างไฟล์ $UploadFile สำเร็จ \n";
                $employee = DB::table('employee')
                    ->select('emp_img')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                // var_dump($employee[0]->emp_img);
                $emp_img = $employee[0]->emp_img;
                // echo "$emp_img";
                Storage::delete($emp_img);
            }else {
                // echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                // echo "เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
                $data = DB::select("UPDATE `employee` SET `emp_img` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                // echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }

            $UploadFile = Storage::disk('local')->putFile("compcard", $file3);
            // echo $UploadFile."\n";
            if ($UploadFile) {
                // echo "สร้างไฟล์ $UploadFile สำเร็จ \n";
                $employee = DB::table('employee')
                    ->select('emp_compcard')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                // var_dump($employee[0]->emp_compcard);
                $emp_compcard = $employee[0]->emp_compcard;
                // echo "$emp_compcard";
                Storage::delete($emp_compcard);
            }else {
                // echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                // echo "เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
                $data = DB::select("UPDATE `employee` SET `emp_compcard` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                // echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }else{
            return response()->json('false'); 
        }

        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status, 'users' => $users);
        return response()->json($report);
    }


    public function updateRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inputs' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $data = $input['inputs'][0];
        $emp_data = $data['emp_data'];
        $user = Auth::user();
        $emp_id = $user->emp_id;
        // $emp_id = $data['emp_id'];
        $Address = $data['inputAddress'];
        $Birthday = $data['inputBirthday'];
        $FirstName = $data['inputFirstName'];
        $Height = $data['inputHeight'];
        $LastName = $data['inputLastName'];
        $NickName = $data['inputNickName'];
        $Sex = $data['inputSex'];
        $Weight = $data['inputWeight'];
        $data = DB::select("UPDATE `employee` SET `emp_name` = '$FirstName', `emp_lastname` = '$LastName',
                 `emp_nickname` = '$NickName', `emp_address`  = '$Address'  , `emp_birthday` = '$Birthday',
                 `emp_weight` = '$Weight', `emp_height` = '$Height', `emp_data` = 1, `emp_sex` = $Sex
                 WHERE `employee`.`emp_id` = $emp_id;");
        
        $json = [
            'success' => true,
            'message' => 'แก้ไขข้อมูลสำเร็จ',
        ];
        return response()->json($json, '200');
    }

    public function updateRegister_M(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inputs' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $data = $input['inputs'][0];
        $emp_data = $data['emp_data'];
        $user = Auth::user();
        $emp_id_M = $user->emp_id;
        $emp_id = $data['emp_id'];
        $Address = $data['inputAddress'];
        $Birthday = $data['inputBirthday'];
        $FirstName = $data['inputFirstName'];
        $Height = $data['inputHeight'];
        $LastName = $data['inputLastName'];
        $NickName = $data['inputNickName'];
        $Sex = $data['inputSex'];
        $Weight = $data['inputWeight'];
        if ($emp_id_M >= 3){
            $data = DB::select("UPDATE `employee` SET `emp_name` = '$FirstName', `emp_lastname` = '$LastName',
                 `emp_nickname` = '$NickName', `emp_address`  = '$Address'  , `emp_birthday` = '$Birthday',
                 `emp_weight` = '$Weight', `emp_height` = '$Height', `emp_data` = 1, `emp_sex` = $Sex
                 WHERE `employee`.`emp_id` = $emp_id;");
        
            $json = [
                'success' => true,
                'message' => 'แก้ไขข้อมูลสำเร็จ',
            ];
            return response()->json($json, '200');
        }else {
            $json = [
                'success' => false,
                'message' => 'คุณไม่มีสิทย์แก้ไขข้อมูล',
            ];
            return response()->json($json, '400');
        }
    
    }

    public function check_empData(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $user = Auth::user();
        $emp_id = $user->emp_id;
        $input['user'] = $user;
        $emp_data = DB::table('employee')
            ->select('emp_data')
            ->where('employee.emp_id', '=', $emp_id)
            ->get();
        $emp_data = $emp_data[0];
        return response()->json($emp_data, '200');
    }

    public function search_oneuser(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $user = Auth::user();
        $emp_id = $user->emp_id;
        $user_status = $user->user_status;
        
        $emp_data = DB::table('employee')
            ->where('employee.emp_id', '=', $emp_id)
            ->get();
        $emp_data = $emp_data[0];
        $json = [
            'success' => true,
            'proflie' => $emp_data,
            'user_status' => $user_status,
        ];

        return response()->json($json, '200');
    }
    
    public function search_oneuser_star(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $user = Auth::user();
        $emp_id = $input['emp_id'];
        $user_status = $user->user_status;
        // ->leftJoin('employee', 'users.emp_id', '=', 'employee.emp_id')
        //         ->leftJoin('star', 'users.emp_id', '=', 'star.emp_id')
        //         ->select('employee.*', 'star.*', 'users.id', 'users.username', 'users.name', 'users.user_status')
        //         ->where('users.user_status', '<=', 2)
        //         ->get();
        if ($user_status >=3){
            $emp_data = DB::table('employee')
                ->leftJoin('star', 'employee.emp_id', '=', 'star.emp_id')
                ->select('employee.*', 'star.*')
                ->where('employee.emp_id', '=', $emp_id)
                ->get();
            $emp_data = $emp_data[0];
            $json = [
                'success' => true,
                'proflie' => $emp_data,
                'user_status' => $user_status,
            ];
            return response()->json($json, '200');
        }else {
            $json = [
                'success' => false,
                'proflie' => null,
                'user_status' => $user_status,
            ];
            return response()->json($json, '400');
        }
    }

    public function updateHeadshot(Request $request)
    {
        // var_dump($request->all());
        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $emp_id = $request->emp_id;
            $UploadFile = Storage::disk('local')->putFile("headshot", $file);
            if ($UploadFile) {
                $users = DB::table('users')
                    ->select('users.emp_id')
                    ->where('users.username', '=', $emp_id)
                    ->get();
                $emp_id = $users[0]->emp_id;
                $employee = DB::table('employee')
                    ->select('emp_headshot')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                $emp_headshot = $employee[0]->emp_headshot;
                Storage::delete($emp_headshot);
            }else {
                echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                $data = DB::select("UPDATE `employee` SET `emp_headshot` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }


        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status);
        return response()->json($report);

    }

    public function updateImg(Request $request)
    {
        // var_dump($request->all());
        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $emp_id = $request->emp_id;
            $UploadFile = Storage::disk('local')->putFile("img", $file);
            if ($UploadFile) {
                $users = DB::table('users')
                    ->select('users.emp_id')
                    ->where('users.username', '=', $emp_id)
                    ->get();
                $emp_id = $users[0]->emp_id;
                $employee = DB::table('employee')
                    ->select('emp_img')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                $emp_img = $employee[0]->emp_img;
                Storage::delete($emp_img);
            }else {
                echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                $data = DB::select("UPDATE `employee` SET `emp_img` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }


        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status);
        return response()->json($report);

    }

    public function updateCompcard(Request $request)
    {
        // var_dump($request->all());
        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $emp_id = $request->emp_id;
            $UploadFile = Storage::disk('local')->putFile("compcard", $file);
            if ($UploadFile) {
                $users = DB::table('users')
                    ->select('users.emp_id')
                    ->where('users.username', '=', $emp_id)
                    ->get();
                $emp_id = $users[0]->emp_id;
                $employee = DB::table('employee')
                    ->select('emp_compcard')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                $emp_compcard = $employee[0]->emp_compcard;
                Storage::delete($emp_compcard);
            }else {
                echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                $data = DB::select("UPDATE `employee` SET `emp_compcard` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }


        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status);
        return response()->json($report);

    }

    public function updateHeadshot_M(Request $request)
    {
        // var_dump($request->all());
        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $emp_id = $request->emp_id;
            $UploadFile = Storage::disk('local')->putFile("headshot", $file);
            if ($UploadFile) {
                // $users = DB::table('users')
                //     ->select('users.emp_id')
                //     ->where('users.username', '=', $emp_id)
                //     ->get();
                // $emp_id = $users[0]->emp_id;
                $employee = DB::table('employee')
                    ->select('emp_headshot')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                $emp_headshot = $employee[0]->emp_headshot;
                Storage::delete($emp_headshot);
            }else {
                echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                $data = DB::select("UPDATE `employee` SET `emp_headshot` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }


        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status);
        return response()->json($report);

    }

    public function updateImg_M(Request $request)
    {
        // var_dump($request->all());
        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $emp_id = $request->emp_id;
            $UploadFile = Storage::disk('local')->putFile("img", $file);
            if ($UploadFile) {
                // $users = DB::table('users')
                //     ->select('users.emp_id')
                //     ->where('users.username', '=', $emp_id)
                //     ->get();
                // $emp_id = $users[0]->emp_id;
                $employee = DB::table('employee')
                    ->select('emp_img')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                $emp_img = $employee[0]->emp_img;
                Storage::delete($emp_img);
            }else {
                echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                $data = DB::select("UPDATE `employee` SET `emp_img` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }


        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status);
        return response()->json($report);

    }

    public function updateCompcard_M(Request $request)
    {
        // var_dump($request->all());
        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $emp_id = $request->emp_id;
            $UploadFile = Storage::disk('local')->putFile("compcard", $file);
            if ($UploadFile) {
                // $users = DB::table('users')
                //     ->select('users.emp_id')
                //     ->where('users.username', '=', $emp_id)
                //     ->get();
                // $emp_id = $users[0]->emp_id;
                $employee = DB::table('employee')
                    ->select('emp_compcard')
                    ->where('employee.emp_id', '=', $emp_id)
                    ->get();
                $emp_compcard = $employee[0]->emp_compcard;
                Storage::delete($emp_compcard);
            }else {
                echo "สร้างไฟล์ $UploadFile ไม่สำเร็จ \n";
            }
            $exists = Storage::disk('local')->exists($UploadFile);
            if ($exists) {
                $data = DB::select("UPDATE `employee` SET `emp_compcard` = '$UploadFile' WHERE `emp_id` = '$emp_id';");
            }else {
                echo "ไม่เจอไฟล์ $UploadFile \n emp_id = $emp_id \n";
            }
        }


        $status = true;
        $report = array('message' => 'อัพโหลดรูปสำเร็จ', 'status' => $status);
        return response()->json($report);

    }


    public function update_star(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'emp_id' => 'required',
            'starRate1' => 'required',
            'starRate2' => 'required',
            'starRate3' => 'required',
            'starRate4' => 'required',
            'starRate5' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $user = Auth::user();
        $status_user = $user->emp_id;
        $emp_id = $input["emp_id"];
        $star_1 = $input["starRate1"];
        $star_2 = $input["starRate2"];
        $star_3 = $input["starRate3"];
        $star_4 = $input["starRate4"];
        $star_5 = $input["starRate5"];
        $comment = $input["comment"];
        
        if ($status_user >= 3){
            DB::select("UPDATE `star` SET  `star_1` = '$star_1', `star_2` = '$star_2', `star_3` = '$star_3', 
                        `star_4` = '$star_4', `star_5` = '$star_5', `star_comment` = '$comment' WHERE `emp_id` = $emp_id;");

            $report = array('message' => 'อัพเดทดาวสำเร็จ', 'status' => true);
            return response()->json($report);
        }else {
            $report = array('message' => 'อัพเดทดาวสำเร็จ', 'status' => false);
            return response()->json($report);
        }
    }

    public function create_tac(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dateEnd' => 'required',
            'dateStart' => 'required',
            // 'insertEmp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $dateEnd = $input["dateEnd"];
        $dateStart = $input["dateStart"];
        $insertEmp = $input["insertEmp"];
        $task_id = DB::select("SELECT MAX(task_id) FROM `task`");
            foreach ($task_id as $key => $value){
                foreach ($value as $key2 => $value2){
                    $max_taskId = $value2;
                }
            }
        $task_id = $max_taskId + 1;
        DB::table('task')->insert([
            [   'task_id' => $task_id, 
                'task_start' => $dateStart,
                'task_end' => $dateEnd,
                'task_status' => 1,
            ]
        ]);
        foreach ($insertEmp as $key => $value){
            // echo $value.'+'.$task_id.', ';
            DB::table('task_user')->insert([
                [   'tu_id' => null, 
                    'emp_id' => $value,
                    'task_id' => $task_id,
                ]
            ]);
        }
        // DB::table('task_user')->insert([
        //     [   'tu_id' => null, 
        //         'emp_id' => 19,
        //         'task_id' => $task_id,
        //     ]
        // ]);

        return response()->json(['message'=>$input], 200);
    }

    public function all_tac(Request $request)
    {
        $tac = DB::table('task')
            ->orderBy('task_start', 'desc')
            ->get();
        foreach ($tac as $key => $object) {
            $task_id = $object->task_id;
            $num_emp = DB::table('task_user')
                ->where('task_id', '=', $task_id)
                ->count();
            $object->numEmp = $num_emp;
        }
        return response()->json(['tac'=>$tac], 200);
    }

    public function tac(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tac_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $tac_id = $input['tac_id'];
        $tac_emp = DB::table('task_user')
            // ->leftJoin('task', 'task_user.task_id', '=', 'task.task_id')
            ->leftJoin('employee', 'task_user.emp_id', '=', 'employee.emp_id')
            ->leftJoin('users', 'users.emp_id', '=', 'employee.emp_id')
            ->leftJoin('star', 'star.emp_id', '=', 'employee.emp_id')
            ->select('task_user.*', 'employee.*', 'users.username', 'star.*')
            ->where('task_id', '=', $tac_id)
            ->get();

        $tac = DB::table('task')
            ->where('task_id', '=', $tac_id)
            ->get();
        return response()->json(['tac_emp'=>$tac_emp, 'tac'=>$tac[0]], 200);
    }
    
    public function notstrike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'date_start' => 'required',
            // 'date_end' => 'required',
            'date' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();

        $date = $input['date'];
        $date_start = $date.'-01';
        $date_end = date('Y-m-t',strtotime($date_start));

        $tac = DB::table('notstrike')
            ->where('notStrike_date', '>=',  $date_start)
            ->where('notStrike_date', '<=',  $date_end)
            ->orderBy('notStrike_date', 'desc')
            ->get();

        $months = array(1=>'มกราคม','กุมภาพันธ์','มีนาคม',
        'เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม',
        'กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
        
        // $day = array(31, 30, 29, 28);
        // $month = date("m");
        // $year = date("Y")+543;
        // for($i=0; $i < count($day); $i++)
        // {
        //     $day_check = $day[$i];
        //     if(checkdate($month, $day_check, $year))
        //     {
        //         //$month=$months[(int)$month];
        //             //$last_date = "$day_check-$month-$year";
        //         $last_date = "$day_check/$month/$year";
        //         break;	
        //     }
        // }
        $test =  date('Y-m-t',strtotime($date_start));

        
        
        // echo $last_date;
        return response()->json(['tac' => $tac, 'test' => $test], 200);
    }

    public function Add_notstrike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'comment' => 'required', //กันError ตอนไม่กรอก comment
            'date' => 'required',
            'status' => 'required',
            'routine' => 'required',
            'still' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $num_dateInput = DB::table('notstrike')
            ->where('notStrike_date', '=', $input['date'])
            ->count();
        // echo $num_dateInput++;
        if ($num_dateInput < 1) {
            DB::table('notstrike')->insert([
                [   'notStrike_comment' => $input['comment'],
                    'notStrike_date' => $input['date'],
                    'notStrike_status' => $input['status'],
                    'notStrike_routine' => $input['routine'],
                    'notStrike_still' => $input['still'],
                ]
            ]);
            $message = 'เพิ่มวันห้ามหยุดสำเร็จ';
        }else {
            $date = $input['date'];
            $task_status = $input['status'];
            $comment = $input['comment'];
            DB::select("UPDATE `notstrike` SET `notStrike_status` = '$task_status', `notStrike_comment` = '$comment' WHERE `notstrike`.`notStrike_date` = '$date';");
            $message = 'วันที่ซ้ำ';
        }
        $notstrike = DB::table('notstrike')
            ->orderBy('notStrike_date', 'desc')
            ->get();
        return response()->json(['notstrike'=>$notstrike, 'message'=>$message, 'num_dateInput'=>$num_dateInput], 200);
    }

    public function de_tac(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tac_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $tac_id = $input['tac_id'];
        $tac = DB::table('task_user')
            ->where('task_id', '=',  $tac_id)
            ->get();
        foreach ($tac as $key => $value){
            // print_r ($value->tu_id);
            $tu_id = $value->tu_id;
            DB::select("DELETE FROM `task_user` WHERE `task_user`.`tu_id` = $tu_id");
        }
        // DB::select("DELETE FROM `task_user` WHERE `task_user`.`task_id` = $tac_id");
        DB::select("DELETE FROM `task` WHERE `task`.`task_id` = $tac_id");
        return response()->json(['tac'=>$tac, 'message'=>'message'], 200);
    }

    public function de_notstrike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $date = $input["date"];
        $test = DB::select("DELETE FROM `notstrike` WHERE `notstrike`.`notStrike_date` = '$date'");
        $message = 'ลบวันห้ามหยุดสำเร็จ';
        $notstrike = DB::table('notstrike')
            ->orderBy('notStrike_date', 'desc')
            ->get();
        return response()->json(['notstrike'=>$notstrike, 'message'=>$date, 'test'=> $test], 200);
    }

    public function notstrike_one(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();

        $date = $input['date'];
        $tac = DB::table('notstrike')
            ->where('notStrike_date', '=', $date)
            ->get();
        
        $num = DB::table('notstrike')
            ->where('notStrike_date', '=', $date)
            ->count();
        if ($num >= 1) {
            $status = true;
            $tac = $tac[0];
        }else {
            $status = false;
            $tac = null;
        }

        return response()->json(['tac' => $tac, 'status' => $status], 200);
    }

    public function settask_start(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'st_month' => 'required',
            'st_tac' => 'required',
            'st_alter' => 'required',
            'st_delegate' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
        $st_month = $input["st_month"];
        $st_tac = $input["st_tac"];
        $st_alter = $input["st_alter"];
        $st_delegate = $input["st_delegate"];
        DB::select("UPDATE `settask_start` SET `st_month` = $st_month,
                `st_tac` = $st_tac, `st_alter` = $st_alter, `st_delegate` = $st_delegate
                 WHERE `st_id` = 1;");
        return response()->json($input, '200');
    }

    public function task_start(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();

        $settask_start = DB::table('settask_start')
            ->where('settask_start.st_id', '=', '1')
            ->get();
        $settask_start = $settask_start[0];
        $st_month = $settask_start->st_month;
        $st_tac = $settask_start->st_tac;
        $st_alter = $settask_start->st_alter;
        $st_delegate = $settask_start->st_delegate;
        return response()->json(['st_month'=>$st_month, 'st_tac'=>$st_tac, 'st_alter'=> $st_alter, 'st_delegate'=> $st_delegate], '200');
    }

    public function tac_changeStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tac_id' => 'required',
            'event' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $tac_id = $input["tac_id"];
        $event = $input["event"];
        if ($event){
            $task_status = 1;
        }else {
            $task_status = 0;
        }
        DB::select("UPDATE `task` SET `task_status` = '$task_status' WHERE `task`.`task_id` = $tac_id;");
        return response()->json(['tac_id'=> $tac_id, 'task_status'=> $event], 200);
    }

    public function num_tac(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $num_tac = DB::table('task')
            ->count();
        return response()->json(['num_tac'=> $num_tac], 200);
    }

    public function calendar_event(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'format' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $date = $input['date'];
        $date_start = $date.'-01';
        $date_end = date('Y-m-t',strtotime($date_start));

        $notstrike = DB::table('notstrike')
            ->where('notStrike_date', '>=',  $date_start)
            ->where('notStrike_date', '<=',  $date_end)
            ->where('notStrike_status', '=',  1)
            ->orderBy('notStrike_date', 'ASC')
            ->get();
        $tac = DB::table('task')
            ->leftJoin('task_user', 'task_user.task_id', '=', 'task.task_id')
            ->leftJoin('employee', 'employee.emp_id', '=', 'task_user.emp_id')
            ->leftJoin('users', 'employee.emp_id', '=', 'users.emp_id')
            ->select(
                'task.*', 
                'employee.emp_name', 
                'employee.emp_lastname', 
                'employee.emp_nickname',
                'users.username'
                )
            ->where('task_start', '>=',  $date_start)
            ->where('task_end', '<=',  $date_end)
            ->where('task.task_status', '=',  1)
            ->orderBy('task_start', 'ASC')
            ->get();

        $test = 'test';
        $tomorrow_date_start = date('m-d-Y',strtotime($date_start . "+1 days"));
        $tomorrow_date_end = date('m-d-Y',strtotime($date_end . "+1 days"));

        // วนลูป สร้าง Array ทีละวัน และ ลบวันหยุดงาน, วันที่ขอหยุด& แทนงาน สำเร็จ
        // foreach ($tac as $key => $value) {
        //     echo $key.' => '.$value->task_start;
        //     print_r ($value);
        //     $task_start = $value->task_start;
        //     $task_end = $value->task_end;
        //     $i = 0;
        //     while ($task_start != $task_end) {
        //         $i++;
        //         $tomorrow = date('Y-m-d',strtotime($task_start . "+1 days"));
        //         $task_start = $tomorrow;
        //         echo $task_start.' != '.$task_end.' || <br>';
        //     }
        // }
        
        
        return response()->json(['tac' => $tac, 'notstrike' => $notstrike], 200);
    }

    public function calendar_event_one(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'format' => 'required',
            'number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $date = $input['date'];
        $date_start = $date.'-01';
        $date_end = date('Y-m-t',strtotime($date_start));
        $number = $input['number'];

        $notstrike = DB::table('notstrike')
            ->where('notStrike_date', '>=',  $date_start)
            ->where('notStrike_date', '<=',  $date_end)
            ->where('notStrike_status', '=',  1)
            ->orderBy('notStrike_date', 'ASC')
            ->get();
        $tac = DB::table('task')
            ->leftJoin('task_user', 'task_user.task_id', '=', 'task.task_id')
            ->leftJoin('employee', 'employee.emp_id', '=', 'task_user.emp_id')
            ->leftJoin('users', 'employee.emp_id', '=', 'users.emp_id')
            ->select(
                'task.*', 
                'employee.emp_name', 
                'employee.emp_lastname', 
                'employee.emp_nickname',
                'users.username'
                )
            ->where('task_start', '>=',  $date_start)
            ->where('task_end', '<=',  $date_end)
            ->where('task.task_status', '=',  1)
            ->where('users.username', '=',  $number)
            ->orderBy('task_start', 'ASC')
            ->get();

        $test = 'test';
        $tomorrow_date_start = date('m-d-Y',strtotime($date_start . "+1 days"));
        $tomorrow_date_end = date('m-d-Y',strtotime($date_end . "+1 days"));
        
        return response()->json(['tac' => $tac, 'notstrike' => $notstrike], 200);
    }

    public function search_number(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $users = DB::table('users')
            ->leftJoin('employee', 'users.emp_id', '=', 'employee.emp_id')
            ->where('users.user_status', '<=',  2)
            ->get();
        return response()->json(['users'=> $users], 200);
    }
        
}