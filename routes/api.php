<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('alluser', 'API\UserController@alluser');
Route::post('allManager', 'API\UserController@allManager');
Route::post('allAdmin', 'API\UserController@allAdmin');
Route::post('sms', 'API\UserController@sms');
Route::post('confirm', 'API\UserController@confirm');
Route::post('confirm2', 'API\UserController@confirm2');
Route::post('newPassApi', 'API\UserController@newPassApi');
Route::post('UploadImg', 'API\UserController@UploadImg');
Route::post('updateHeadshot', 'API\UserController@updateHeadshot');
Route::post('updateImg', 'API\UserController@updateImg');
Route::post('updateCompcard', 'API\UserController@updateCompcard');
Route::post('updateHeadshot_M', 'API\UserController@updateHeadshot_M');
Route::post('updateImg_M', 'API\UserController@updateImg_M');
Route::post('updateCompcard_M', 'API\UserController@updateCompcard_M');

Route::post('test', 'API\UserController@test2');



Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserController@details');
	Route::post('logout', 'API\UserController@logout');
	Route::post('createUser', 'API\UserController@createUser');

	Route::post('de_user', 'API\UserController@de_user');
	Route::post('up_Status', 'API\UserController@up_Status');
	Route::post('down_Status', 'API\UserController@down_Status');

	Route::post('check_empData', 'API\UserController@check_empData');
	Route::post('search_oneuser', 'API\UserController@search_oneuser');
	Route::post('search_oneuser_star', 'API\UserController@search_oneuser_star');

	Route::post('testToken', 'API\UserController@testToken');

	Route::post('allEmp', 'API\UserController@allEmp');
	Route::post('allEmp_table', 'API\UserController@allEmp_table');

	Route::post('updateRegister', 'API\UserController@updateRegister');
	Route::post('updateRegister_M', 'API\UserController@updateRegister_M');
	Route::post('update_star', 'API\UserController@update_star');

	Route::post('create_tac', 'API\UserController@create_tac');
	Route::post('all_tac', 'API\UserController@all_tac');
	Route::post('tac', 'API\UserController@tac');
	Route::post('de_tac', 'API\UserController@de_tac');
	Route::post('notstrike', 'API\UserController@notstrike');
	Route::post('Add_notstrike', 'API\UserController@Add_notstrike');
	Route::post('notstrike_one', 'API\UserController@notstrike_one');
	Route::post('de_notstrike', 'API\UserController@de_notstrike');

	Route::post('settask_start', 'API\UserController@settask_start');
	Route::post('task_start', 'API\UserController@task_start');
	Route::post('tac_changeStatus', 'API\UserController@tac_changeStatus');
	Route::post('num_tac', 'API\UserController@num_tac');
	Route::post('calendar_event', 'API\UserController@calendar_event');
	Route::post('calendar_event_one', 'API\UserController@calendar_event_one');
	Route::post('search_number', 'API\UserController@search_number');
});