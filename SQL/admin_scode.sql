-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2018 at 12:43 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_scode`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(5) NOT NULL COMMENT 'id พนักงาน',
  `EnNo` int(5) NOT NULL COMMENT 'รหัสลายนิ้วมือ',
  `emp_sex` int(1) NOT NULL COMMENT 'เพศ 0.หญิง 1.ชาย',
  `emp_name` varchar(191) NOT NULL COMMENT 'ชื่อจริง',
  `emp_lastname` varchar(191) NOT NULL COMMENT 'นามสกุล',
  `emp_nickname` varchar(191) NOT NULL COMMENT 'ขื่อเล่น',
  `emp_position` int(5) NOT NULL COMMENT 'ตำแหน่งงาน',
  `emp_address` text NOT NULL COMMENT 'ที่อยู่',
  `emp_birthday` date NOT NULL COMMENT 'วันเกิด',
  `emp_weight` float(6,2) NOT NULL COMMENT 'น้ำหนัก',
  `emp_height` float(6,2) NOT NULL COMMENT 'ส่วนสูง',
  `emp_img` varchar(191) NOT NULL COMMENT 'ที่อยู่รูปภาพ',
  `emp_routine` tinyint(1) NOT NULL COMMENT 'สถานะ 0.พนักงานชั่วคราว 1.พนักงานประจำ',
  `emp_still` tinyint(1) NOT NULL COMMENT 'สถานะ 0.ไม่ทำงานแล้ว 1.ยังทำงานอยู่',
  `emp_data` tinyint(1) NOT NULL COMMENT 'สถานะเพิ่มข้อมูล'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `new_user`
--

CREATE TABLE `new_user` (
  `id` int(11) NOT NULL COMMENT 'ไอดี',
  `phone_number` varchar(191) NOT NULL COMMENT 'เบอร์โทร',
  `confirm` varchar(191) NOT NULL COMMENT 'รหัสยืนยันตัวตน (URL)',
  `dalete` tinyint(1) NOT NULL COMMENT 'ลบข้อมูล'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `new_user`
--

INSERT INTO `new_user` (`id`, `phone_number`, `confirm`, `dalete`) VALUES
(1, '0844564787', 'RcaOMM7onY', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('007ee1bee3ed27b39258f7814230bc82d61c78bd31511f7d0aef7d6a0ffc38861487a8744d5eedc6', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:04:10', '2018-10-03 02:04:10', '2019-10-03 09:04:10'),
('025ac6b7c99115b267880db5c6e110930485073eba09ef601f7ba86616cf49e85841975061668cb9', 5, 1, 'MyApp', '[]', 0, '2018-10-03 00:49:29', '2018-10-03 00:49:29', '2019-10-03 07:49:29'),
('1283bb6ae415eea0f15db4ecb3830ba2adeb83fe82d5840cbdcecce021377540fb3e16f6dd3dea0f', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:48:03', '2018-10-03 01:48:03', '2019-10-03 08:48:03'),
('12b119f21713a51f9cdd407e68a05fb618d5238f59a1362eefb303f1e53e603e004444308f1241c2', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:00:06', '2018-10-03 02:00:06', '2019-10-03 09:00:06'),
('1539fb91ea2cd6804b5736f059b5a30e3b1f00e3e4ae212dd0de29dc05f1defd7f18552fc145daf7', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:19:00', '2018-10-04 20:19:00', '2019-10-05 03:19:00'),
('1841df75a1d85935bd99629f230d285b18cf724d341c88ba3cf38d15a0da083cf90a114b36bd3cf4', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:11:31', '2018-10-04 20:11:31', '2019-10-05 03:11:31'),
('1c39638d9c825768a0a0e4b783710b6a715a080bcce49896e5fe0dd29befc5c28a5a520725277095', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:15:41', '2018-10-04 20:15:41', '2019-10-05 03:15:41'),
('2484d7c894876f6653b5ea5436d162c6eb7738c7789245705d47f75133b8401bec32b1d7b4170aaf', 5, 1, 'MyApp', '[]', 0, '2018-10-17 21:06:00', '2018-10-17 21:06:00', '2019-10-18 04:06:00'),
('29917a1629a98922d768e8d1048fbdc768a1004cef1efc12eabc3fb341cccfb893775c5cd3d133bb', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:59:36', '2018-10-03 01:59:36', '2019-10-03 08:59:36'),
('2c6d2af60e0965587efbaa644c8126e35b4d335ee1441fc6756deb41c435800cb9aeaba758d58d54', 5, 1, 'MyApp', '[]', 0, '2018-10-07 23:46:47', '2018-10-07 23:46:47', '2019-10-08 06:46:47'),
('2d95246e75567f2f2cf31d7c6108e10253c54e08956492b3092ad1d17ab61c7e6c0fc43f2a1cada0', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:05:56', '2018-10-03 02:05:56', '2019-10-03 09:05:56'),
('2e4dafe200e905a95b3af998223b9161871a709c5eba0afe9da607584e6171783454d75824f9c415', 5, 1, 'MyApp', '[]', 0, '2018-10-07 23:53:35', '2018-10-07 23:53:35', '2019-10-08 06:53:35'),
('2ecdb399701a5d194729a6e6c539fe48a82519f0e43edeed2ce8c48fc26122c7960f0aef9ef2d0ee', 26, 1, 'MyApp', '[]', 0, '2018-10-08 00:54:38', '2018-10-08 00:54:38', '2019-10-08 07:54:38'),
('2f65670b4f98544061016a2bc72827885b7edb098f9cc9f9312ff34b99b275ca6772938a6955f160', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:36:49', '2018-10-04 20:36:49', '2019-10-05 03:36:49'),
('30cebcbc927dea03e15793016c777d3e79e4980958faa38cceb775569266cf0f342093edebbcb8fd', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:49:35', '2018-10-03 01:49:35', '2019-10-03 08:49:35'),
('322240e2404881b1f356d1cc2398c12826c28dadfe298a399dabf3a50e2f80ab5fbe4b8f369f127c', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:59:54', '2018-10-03 01:59:54', '2019-10-03 08:59:54'),
('32e78942f7fa0de4264b1f38d984b40953e71f0d05f5e099526a771a243d3f87c3bef172979d435c', 5, 1, 'MyApp', '[]', 0, '2018-10-08 00:54:51', '2018-10-08 00:54:51', '2019-10-08 07:54:51'),
('3633270d7f91e4bcc754bc1cf17f6a80689fd9d38ed1b05944447ada9c7b71d3d677d0b322f81c6f', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:06:35', '2018-10-03 02:06:35', '2019-10-03 09:06:35'),
('3b7df29bd21d101d321f6ec8818ae33d62a07a0776067f160418f0d849bdc33196c470a353b147a8', 5, 1, 'MyApp', '[]', 0, '2018-10-07 23:53:51', '2018-10-07 23:53:51', '2019-10-08 06:53:51'),
('3e6957944fdb31be7c63f9d3b94d012efa37d198bfefc3dccec2252e964b4adec3190ffd9fb38e67', 30, 1, 'MyApp', '[]', 0, '2018-10-16 01:30:51', '2018-10-16 01:30:51', '2019-10-16 08:30:51'),
('3f839075ce535f4dce07fb62aa2b20f77634c0dcd51e5422196324615a3caab1e277fd92d95416e4', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:06:44', '2018-10-03 02:06:44', '2019-10-03 09:06:44'),
('40c8dcdfee3bc98b78fd1c5cc79231b73a49470410bf0d6d73fd2398ab8f8eff652291b8a6acb22d', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:01:00', '2018-10-03 02:01:00', '2019-10-03 09:01:00'),
('45e96de62cc5a798b1815e81af356467aca7972a1e6c69c278bfbf4a6865f8e63c1fc25d1673d562', 30, 1, 'MyApp', '[]', 0, '2018-10-08 02:31:26', '2018-10-08 02:31:26', '2019-10-08 09:31:26'),
('474d475420fce8ed0dc7e2d74ef530781048bd31aef0724811d3ee62d62aea5ff5a931f22e1f7174', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:49:44', '2018-10-03 01:49:44', '2019-10-03 08:49:44'),
('514c0923451eeac40c8b215bfa55b9bf6d84c33cb2b8f571c97a4b5d618abff2c681d9dece188cae', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:12:31', '2018-10-03 02:12:31', '2019-10-03 09:12:31'),
('53b43b078f411075c26bc2d4674bb332cf093f94c2fbd68117e74a203f09dec9f1dc57a9aa5c4f59', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:00:01', '2018-10-03 02:00:01', '2019-10-03 09:00:01'),
('5aaec0c3dfa67fa5674acb0194362edac4a46d0c82f80518624b9ade4f49a4d3f77ec654998e92ff', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:03:16', '2018-10-03 02:03:16', '2019-10-03 09:03:16'),
('5d8164f285e025f31eb4f4dbcd32fcc5c738750ecaa42def4cb934eeba45b118a4cfb66a3198a3ee', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:15:47', '2018-10-03 02:15:47', '2019-10-03 09:15:47'),
('5fa0109f37c4a4d550e276d6cec1082453f2dc859d973c31816afb3928e69dd0d638544e360c6e3d', 27, 1, 'MyApp', '[]', 0, '2018-10-08 00:54:31', '2018-10-08 00:54:31', '2019-10-08 07:54:31'),
('663bc3f13169a57cd51a7394e5fc652c94f08311062001b1ff75985fd8c7df08bdf7d5fd4c2dd6f0', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:17:50', '2018-10-03 02:17:50', '2019-10-03 09:17:50'),
('6673ef63f97aa8d02463ed8d4831d401fa9e40aee5c024191d7a10e838e713268d93532c2e40e3f8', 29, 1, 'MyApp', '[]', 0, '2018-10-08 00:55:43', '2018-10-08 00:55:43', '2019-10-08 07:55:43'),
('8266bef1a0aa8b885c994f6f0af71a72a6e3d71dd0799b1c93a47744c23df1b1cd08dc720498f8c4', 5, 1, 'MyApp', '[]', 0, '2018-10-07 23:50:22', '2018-10-07 23:50:22', '2019-10-08 06:50:22'),
('894435416b7642d090b4c0a172fd7594bfb2bd66b901c1f521703f256634eb8db633730635f70c41', 5, 1, 'MyApp', '[]', 0, '2018-10-07 23:50:40', '2018-10-07 23:50:40', '2019-10-08 06:50:40'),
('8c04356696fe8335a79299f06a4e4edf8f63f3567f0ff736ae31cbbf3e9491a6e2e2bce2155c0ac2', 5, 1, 'MyApp', '[]', 0, '2018-10-18 21:43:32', '2018-10-18 21:43:32', '2019-10-19 04:43:32'),
('910d14542e5e4a6846ca842f709bedf3248fa17e8a61c9a98c008cd7b5e48a7c121e35bc79545ffe', 5, 1, 'MyApp', '[]', 0, '2018-10-17 21:54:06', '2018-10-17 21:54:06', '2019-10-18 04:54:06'),
('93098a21576262203fb330c35906635a7ff59c666e1430b5caddf9a1cc3209c21a6eea4f477c13ee', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:08:15', '2018-10-03 02:08:15', '2019-10-03 09:08:15'),
('9eb7f09b960ab9c841ac9420614357da16cd1bb0d6eb50e97c62c1e5bff2757a71dbeba51f3d9f6a', 26, 1, 'MyApp', '[]', 0, '2018-10-08 00:54:17', '2018-10-08 00:54:17', '2019-10-08 07:54:17'),
('a3c87f4ab9cab09428b1068ac0d2e648e733dbe396b9d84d5403fa5b4e9710ba463e218b39b02bf0', 28, 1, 'MyApp', '[]', 0, '2018-10-08 00:55:10', '2018-10-08 00:55:10', '2019-10-08 07:55:10'),
('a8b234fa6247071b9c3d3dbf20bd9b90d8054d5b711d586d7159256b3472412cb2264219f12a3115', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:58:39', '2018-10-03 01:58:39', '2019-10-03 08:58:39'),
('a8b8d200f12c3230d01d730f0c2f722b0fc906bc3cfc7583270e54da4c5cb5ca8ee294a2177495c1', 5, 1, 'MyApp', '[]', 0, '2018-10-08 02:54:06', '2018-10-08 02:54:06', '2019-10-08 09:54:06'),
('ac1e930f92949d3e8e7b42678c724eaf1005a3262297a3166dfa53745f100f0c5f4601da094c7c09', 28, 1, 'MyApp', '[]', 0, '2018-10-08 02:32:06', '2018-10-08 02:32:06', '2019-10-08 09:32:06'),
('aeacf54963fbd7ef31cd3e87dab584cc559c5c25e3d45332ab7a3390e70542f01b31a24d591f76b9', 26, 1, 'MyApp', '[]', 0, '2018-10-15 21:04:48', '2018-10-15 21:04:48', '2019-10-16 04:04:48'),
('b147d8131ea9fa2bc27adcb10f0aa18c859984662797552e39bac51a60b670592343078a446ac09b', 5, 1, 'MyApp', '[]', 0, '2018-10-16 20:54:31', '2018-10-16 20:54:31', '2019-10-17 03:54:31'),
('b9cc19a9fefb98c9c27b4c760edffbf5fa7f1e7563a775c828fc1e9903338ddce854870fc249a8ac', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:25:07', '2018-10-03 01:25:07', '2019-10-03 08:25:07'),
('bb5267af3dc78d4d5219110c5d819f0cf588d056fbc67df8ee62860ceabf5b2db01393286969567d', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:59:45', '2018-10-03 01:59:45', '2019-10-03 08:59:45'),
('caf9ed32e26298105b3be62d1c9ab4f07b7161920797096b0314a8dfa6320dd92dc108f8fa1e4fb3', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:21:04', '2018-10-04 20:21:04', '2019-10-05 03:21:04'),
('cd9681f672b6f114a31b2573c046947fa051dc04c923d2da77da1bc711be184ef7f93bbbb7a06ffb', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:18:35', '2018-10-04 20:18:35', '2019-10-05 03:18:35'),
('d1b85af24ab102271298842772d539ef2ac6f68779ad51b0b6d23c726b02cd72486c9cf29d497e98', 29, 1, 'MyApp', '[]', 0, '2018-10-15 21:06:04', '2018-10-15 21:06:04', '2019-10-16 04:06:04'),
('da308a003e2988b0a0aa302b895174089262e3e9014a9aace03a9b69894c743a454ad8c29bb78685', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:08:49', '2018-10-04 20:08:49', '2019-10-05 03:08:49'),
('db719300e2b3cc7d413f7fd439058167e91fbe8b882abc04ec3185645bfde5cb650d764b0b0b7986', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:08:04', '2018-10-03 02:08:04', '2019-10-03 09:08:04'),
('dc9f7df565a86ab6bb6ad16d09a0a975a1a6cb26efd8ffcbd29bf2c1913416932426312d3d7d35a5', 5, 1, 'MyApp', '[]', 0, '2018-10-03 03:43:19', '2018-10-03 03:43:19', '2019-10-03 10:43:19'),
('ded3410be839ad7cc195f185332af6a0b8f8aac3dab0cabd0dddcfa64177c62fa2150e158abb74b2', 5, 1, 'MyApp', '[]', 0, '2018-10-03 02:08:24', '2018-10-03 02:08:24', '2019-10-03 09:08:24'),
('ea7df1b1e4d6c9f44b73ad1bf17b773d1731b845b91a8668ad5518403f9293bd4a0a762a567bdd8c', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:49:17', '2018-10-03 01:49:17', '2019-10-03 08:49:17'),
('ed303f315386f5d87fcde5021479c8e27327b68fc68c51dd550b87701ffeca1c50adb7ab65cc5f26', 29, 1, 'MyApp', '[]', 0, '2018-10-08 02:47:48', '2018-10-08 02:47:48', '2019-10-08 09:47:48'),
('f1801f10b13077f11814843c9c89272f0ba17ec52dae5563514f5a1d4083c1b68626cdad3bf7f3aa', 5, 1, 'MyApp', '[]', 0, '2018-10-17 21:16:51', '2018-10-17 21:16:51', '2019-10-18 04:16:51'),
('f2ddbd315ff1b98312fcacc6c5068ef64ed6b37019b8e4ef8c3b6d3b4a1adcf7f43afb262a65be00', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:12:41', '2018-10-04 20:12:41', '2019-10-05 03:12:41'),
('f7e98a9c92709bd077f654d44c8ea845d75733a8ff1927e7715f63b31eecfaab4c992494425ffadf', 5, 1, 'MyApp', '[]', 0, '2018-10-04 20:05:10', '2018-10-04 20:05:10', '2019-10-05 03:05:10'),
('fb612032a125fa2a1533e506639691887a0907ac5ecfad090108e351edff02c12e59ea7eb941bbc1', 5, 1, 'MyApp', '[]', 0, '2018-10-03 01:59:46', '2018-10-03 01:59:46', '2019-10-03 08:59:46');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'DvdwyioUa5AOLtKJHsYKq5aU0Z519V7cukU6B0cs', 'http://localhost', 1, 0, 0, '2018-10-01 00:19:39', '2018-10-01 00:19:39'),
(2, NULL, 'Laravel Password Grant Client', '4XDVgJdP7fLulWUlm57ef7vAi2An418IXv0QZOQN', 'http://localhost', 0, 1, 0, '2018-10-01 00:19:39', '2018-10-01 00:19:39');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-10-01 00:19:39', '2018-10-01 00:19:39');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `pos_id` int(5) NOT NULL,
  `pos_name` varchar(30) NOT NULL COMMENT 'ตำแหน่งงาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `re_number` varchar(20) NOT NULL COMMENT 'เบอร์โทร',
  `re_confirm` varchar(191) DEFAULT NULL COMMENT 'รหัสยืนยัน',
  `re_delete` tinyint(1) NOT NULL COMMENT 'ลบ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_status` int(1) DEFAULT NULL COMMENT 'สถานะ 1 Employee, 2 Supervisor, 3 Manager, 4 Admin	',
  `emp_id` int(5) DEFAULT NULL COMMENT 'id ผู้ใช้งาน',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `user_status`, `emp_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'test', '0855988010', '$2y$10$SdNdEhpHCZ71mqiV1nJ.NumvlAq6aBVZR.5cnNYQSiDvJM9Y69XUq', 4, 1, NULL, '2018-10-01 02:14:24', '2018-10-01 02:14:24'),
(26, 'test1', '0811111111', '$2y$10$OxnCsfK2eRfGy72nOcQNUe58xFLGaDoAftzhRrEzwSOebXoaoGMJK', 2, NULL, NULL, '2018-10-08 00:54:17', '2018-10-08 00:54:17'),
(27, 'test2', '0822222222', '$2y$10$rkHUPdV89SmPeGCnhgl6Cevw8tlTedBJCQugysxO.filbmpyiX5zm', 2, NULL, NULL, '2018-10-08 00:54:31', '2018-10-08 00:54:31'),
(28, 'test3', '0833333333', '$2y$10$77o/kDaoo2kSFU23wxHMU.dsQ4z/4PvIQRnu6V7vBOPE82IMLw6AK', 3, NULL, NULL, '2018-10-08 00:55:10', '2018-10-08 00:55:10'),
(29, 'test4', '0844444444', '$2y$10$kla4JiNyPZxhK8pw4eYMUu9tQVcan3fGioLAkvLixTTxDalZqCb2e', 4, NULL, NULL, '2018-10-08 00:55:43', '2018-10-08 00:55:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_user`
--
ALTER TABLE `new_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`re_number`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id พนักงาน';

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `new_user`
--
ALTER TABLE `new_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `pos_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
